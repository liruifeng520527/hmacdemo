package com.demo.hmacdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HmacdemoApplication {

    public static void main(String[] args) {

        SpringApplication.run(HmacdemoApplication.class, args);
    }

}
