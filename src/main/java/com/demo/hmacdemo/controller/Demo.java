package com.demo.hmacdemo.controller;

import com.demo.hmacdemo.utils.HmacSha1Signature;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/demo",produces ={MediaType.APPLICATION_JSON_VALUE})
public class Demo {

    @PostMapping("/demo")
    public String demo(@RequestBody String data){
        String key= "sO8UTM5KE9wX5jb5";

        String s = HmacSha1Signature.calculateRFC1024HMAC(data, key);

        return s;
        //System.out.println(data);


    }
}
